package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {


	static BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(System.out));
	static Scanner lector = new Scanner(System.in);
	public static void main(String[] args) {



		ClienteReq cliente = new ClienteReq(escritor, lector);
		int opcion = -1;

		while (opcion != 0) {
			try {
				escritor.write("---------------Cliente Pruebas Proyecto 2---------------\n");
				escritor.write("Ingrese un numeral\n");
				escritor.write("Opciones:\n");
				escritor.write("1: Menú requerimientos \n");
				escritor.write("0: Salir\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: cliente.pruebas(); break;
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}

		try {
			escritor.write("Chao");
			escritor.flush();
			escritor.close();
			lector.close();
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}


	



}