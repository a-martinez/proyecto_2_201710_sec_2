package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

import API.SistemaRecomendacion;


public class ClienteReq {

	BufferedWriter escritor;
	Scanner lector;


	private SistemaRecomendacion sr;

	public ClienteReq(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}

	public void pruebas() {
		int opcion = -1;

		sr = new SistemaRecomendacion();


		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Requerimientos:\n");
				escritor.write("1: Registrar solicitud de recomendación a un usuario. (R1) \n");
				escritor.write("12: Registrar solicitud de recomendación mediante archivo .JSON (R1) \n");
				escritor.write("2: Generar archivo de respuesta a solicitud de usuarios. (R2) \n");
				escritor.write("3: Generar listado ordenado por fecha de peliculas en un género específico dentro de un rango de fechas. (R3) \n");
				escritor.write("4: Añadir un nuevo rating a una película y guardar el error de predicción (R4) \n");
				escritor.write("5: Generar un informe con toda la información que se tiene sobre un usuario y su interacción con las películas. (R5) \n");
				escritor.write("6: Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.(R6) \n");
				escritor.write("7: Ordenar las películas del catálogo en un árbol binario balanceado y ordenarlo por el año de la película. (R7) \n");
				escritor.write("8: Realizar un reporte para describir los segmentos de los usuarios.(R8) \n");
				escritor.write("9: Buscar películas de un género cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado por día /mes /año inicial y día /mes /año final (R9) \n");
				escritor.write("10: Obtener las N películas de mayor prioridad en su orden. (R10) \n");
				escritor.write("11: Generar lista de peliculas de acuerdo a los 3 criterios ingresados por el usuario (R11) \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: r4(); break;
				case 5: r5(); break;
				case 6: r6(); break;
				case 7: r7(); break;
				case 8: r8(); break;
				case 9: r9(); break;
				case 10: r10(); break;
				case 11: r11(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private void r1() throws IOException{

		//TODO: Registrar solicitud de recomendación a un usuario. Recuerde que hay dos tipos de solicitud
		//RECUERDE: Usted debe manejar si el parametro es por usuario con ID o la ruta del JSON.
		Integer usuarioID = -1;
		String rutaJson = "";

		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		try{
		usuarioID = Integer.parseInt(lector.next());
		} catch (Exception e) {

		}
		System.out.println(usuarioID);
		// opción por JSON:
		//escritor.write("Ingrese la ruta JSON: \n");
		//escritor.flush();
		rutaJson = lector.next();
		//System.out.println(rutaJson);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		sr.registrarSolicitudRecomendacion( usuarioID, rutaJson);
		

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r2() throws IOException{

		sr.generarRespuestasRecomendaciones();

		long tiempo = System.nanoTime();

		String ruta = sr.getUltimaRuta();
		tiempo = System.nanoTime() - tiempo;
		escritor.write("El archivo se creó con la ruta: " + ruta + "\n");
		escritor.write("\n");

		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r3() throws NumberFormatException, IOException
	{
		
		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);
		sr.peliculasGeneroPorFechas(genero, new Date(fechaInicial), new Date(fechaFinal));
	
		long tiempo = System.nanoTime();

  
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r4() throws IOException
	{
	
		escritor.write("Ingrese el ID del Usuario: \n");
		escritor.flush();
		String idUsuario = lector.next();
		System.out.println(idUsuario);

		escritor.write("Ingrese el ID de la pelicula: \n");
		escritor.flush();
		String idPelicula = lector.next();
		System.out.println(idPelicula);

		escritor.write("Ingrese el nuevo Rating \n");
		escritor.flush();
		String rating = lector.next();
		System.out.println(rating);

		long tiempo = System.nanoTime();

		sr.agregarRatingConError(Integer.parseInt(idUsuario), Integer.parseInt(idPelicula), Double.parseDouble(rating));

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r5() throws IOException{

 
		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		System.out.println(usuarioID);

		long tiempo = System.nanoTime();

		sr.informacionInteraccionUsuario(Integer.parseInt(usuarioID));

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r6() throws IOException{

		//TODO:Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.
		//RECUERDE: 4 tipos o segmentos diferentes: Inconformes, Conformes, Neutrales y No Clasificado.


		long tiempo = System.nanoTime();

		//TODO: Llamar método clasificarUsuariosPorSegmento() del API
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r7() throws IOException{

		//TODO:ordenar las películas del catálogo en un árbol binario balanceado y ordenado por el año de la película.

		//RECUERDE:Dentro de cada nodo del árbol, se tiene una lista, en la que en cada nodo se tiene una película y 
		//una tabla de hash con los usuarios que han asignado un tag específico a dicha película.



		long tiempo = System.nanoTime();

		//TODO: Llamar método ordernarPelicuaPorAnho() del API

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r8() throws IOException{

		//TODO:  Dado un segmento se quiere saber lo siguiente: el error promedio, sus 5 géneros con más cantidad de ratings y sus 5 géneros con mejor rating promedio.
		//RECUERDE: Error promedio = suma de errores sobre sus ratings dividido la cantidad de ratings con error asociado. 

		escritor.write("Ingrese el segmento: \n");
		escritor.flush();
		String segmento = lector.next();
		System.out.println(segmento);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método generarReporteSegmento(String segmento) del API 
		//Se espera como resultado: VOReporteSegmento 
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r9() throws NumberFormatException, IOException
	{
		//TODO: Buscar películas de un género cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado
		//RECUERDE: día /mes /año inicial – día /mes/ año final, usando el mismo formato de fecha que aparece en los datos de las películas
		//RECUERDE: Tenga en cuenta que para las películas, su mes de lanzamiento (campo Released) está dado por las 3 iniciales en inglés (Jan, Feb, Mar, Apr, ...). 

		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar método peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API 
		//Se espera como resultado: lista VOPelicula		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r10() throws IOException{

		//TODO: A partir del Heap se quiere obtener las N películas de mayor prioridad en su orden, donde el valor N es dado por el usuario.
		//RECUERDE:  Para cada película resultante, hay que mostrar su título, año, votos totales, promedio anual votos y prioridad de clasificación. 

		escritor.write("Ingrese el N a buscar en el SR.\n");
		escritor.flush();
		int peliculas = lector.nextInt();
		System.out.println(peliculas);

		//RECUERDE: Utilice la variable previamente declarada y adaptela al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método peliculasMayorPrioridad(int n) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r11() throws IOException{

		//TODO: Las películas resultantes deben obtenerse en el siguiente orden: Inicialmente ordenadas por año. 
		//Para las películas del mismo año, deben ordenarse por país (alfabéticamente). 
		//Para películas del mismo país deben ordenarse por género. Para películas del mismo género deben ordenarse por rating IMBD. 
		//Tenga en cuenta que una película puede tener asociado múltiples países y múltiples géneros.

		//RECUERDE: los criterios pueden ser dados en cualquier orden para el filtro. 
		escritor.write("Ingrese criterio1: \n");
		escritor.flush();
		String criterio1 = lector.next();
		System.out.println(criterio1);

		escritor.write("Ingrese criterio2: \n");
		escritor.flush();
		String criterio2 = lector.next();
		System.out.println(criterio2);

		escritor.write("Ingrese el criterio3: \n");
		escritor.flush();
		String criterio3 = lector.next();
		System.out.println(criterio3);

		//RECUERDE: Utilice las variables previamente declaradas y adaptelas al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar método consultarPeliculasFiltros (Integer anho, String pais, VOGeneroPelicula genero) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}







}
