package logica;

import java.util.Comparator;

import API.SistemaRecomendacion;
import estructuras.Lista;
import vos.VOPelicula;
import vos.VORating;
import vos.VOSimilitud;
import vos.VOUsuario;

public class Recomendacion extends Thread{
	
	private VOUsuario u;
	private Lista<VOSimilitud> r;
	boolean l;
	
	
	
	public Recomendacion(VOUsuario u, Lista<VOSimilitud> r, boolean l) {
		super();
		this.u = u;
		this.r = r;
		this.l = l;
	}

	@Override
	public void run() {
		Lista<VOSimilitud> recomendaciones = new Lista<>();
		for (VOPelicula pe : SistemaRecomendacion.getPeliculas().valores()) {
			double arriba = 0;
			double abajo = 0;
			for(VORating r: u.getRatings()){
					double sim = pe.getSimilitudes().get(r.getIdPelicula()).getSimilitud();
					arriba += (sim*r.getRating());
					abajo += sim;
			}
			recomendaciones.add(new VOSimilitud(pe.getIdPelicula(), (arriba/abajo)));
		}
		if(l){
			r=recomendaciones;
		}else{
			MergeSort.sort(recomendaciones, new Comparator<VOSimilitud>() {
				@Override
				public int compare(VOSimilitud o1, VOSimilitud o2) {
					if(o1.getSimilitud()>o2.getSimilitud()){
						return -1;
					}
					if(o1.getSimilitud()<o2.getSimilitud()){
						return 1;
					}
					return 0;
				}
			});
			for(int i=0; i<5;i++){
				r.add(recomendaciones.get(i));
			}
		}
	}

}
