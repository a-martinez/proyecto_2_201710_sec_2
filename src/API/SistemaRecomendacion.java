package API;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import estructuras.ArbolBalanceado;
import estructuras.EncadenamientoSeparadoTH;
import estructuras.Lista;
import estructuras.MaxHeapCP;
import estructuras.Queue;
import logica.Recomendacion;
import vos.VOGeneroPelicula;
import vos.VOPelicula;
import vos.VORating;
import vos.VOSimilitud;
import vos.VOTag;
import vos.VOUsuario;
import vos.VOUsuarioPelicula;


public class SistemaRecomendacion {

	private static EncadenamientoSeparadoTH<Integer, VOPelicula> peliculas;
	private static EncadenamientoSeparadoTH<Integer, VOUsuario> usuarios;
	private static EncadenamientoSeparadoTH<String, ArbolBalanceado<Date, VOPelicula>> generos;
	private	static MaxHeapCP<VOUsuario> cola = new MaxHeapCP<>();
	private int tags;
	private String ultimaRuta = "";
	
	public String getUltimaRuta() {
		return ultimaRuta;
	}

	public SistemaRecomendacion() {
		try{
			leerPeliculas();
		} catch (Exception b) {
			peliculas = new EncadenamientoSeparadoTH<>(1361);
			usuarios = new EncadenamientoSeparadoTH<>(163);
			generos = new EncadenamientoSeparadoTH<>(3);
			cargarPeliculas("data/links_json.json");
			cargarRatings("data/ratings.csv",true);
			cargarTags("data/tags.csv",true);
			cargarGeneros();

			for(VOPelicula i: peliculas.valores()){
				SimilitudCoseno(i);
			}
			try(ObjectOutputStream out1 = new ObjectOutputStream(new FileOutputStream(new File("data/Peliculas.cr")))){
				out1.writeObject(peliculas);
				out1.flush();
				out1.reset();
				out1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		generos = new EncadenamientoSeparadoTH<>(3);
		usuarios = new EncadenamientoSeparadoTH<>(163);
		cargarRatings("data/ratings.csv", false);
		cargarTags("data/tags.csv", false);
		cargarGeneros();
	}

	private void leerPeliculas() throws Exception{
		ObjectInputStream inP = new ObjectInputStream(new FileInputStream("data/Peliculas.cr")); 
		peliculas = (EncadenamientoSeparadoTH<Integer, VOPelicula>) inP.readObject();
		inP.close();
	}
	
	private void cargarPeliculas(String ruta){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				JSONObject jsonObject1 = (JSONObject)jsonObject.get("imdbData");
				int id = Integer.parseInt((String)jsonObject.get("movieId"));
				String[] generos = ((String)jsonObject1.get("Genre")).split(",");
				Date date;
				try{
					date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH).parse((String)jsonObject1.get("Released"));
				}
				catch(Exception e){
					try{
						date = new SimpleDateFormat("yyyy").parse((String)jsonObject1.get("Year"));
					}catch(Exception n){
						date = new SimpleDateFormat("yyyy").parse("0000");
					}
				}

				peliculas.insertar(id, new VOPelicula(id, (String)jsonObject1.get("Title"), 0.0, date, new Lista<>(), generos, new Lista<>(), new Lista<>()));     
			}
		} catch (Exception e) {
		}
	}

	private void cargarRatings(String rutaRatings, boolean q) {
		try(BufferedReader br = new BufferedReader(new FileReader(rutaRatings))){
			String linea = br.readLine();
			while((linea = br.readLine())!=null){
				try{
					String[] w = linea.split(",");
					int a = Integer.parseInt(w[0]);
					int b = Integer.parseInt(w[1]);
					double c = Double.parseDouble(w[2]);
					long d = Long.parseLong(w[3]);
					Date date = new Date(d);
					VORating rating = new VORating(a, b, c, d);
					if(q){peliculas.darValor(b).getRatings().add(rating);}
					if(!usuarios.tieneLlave(a)){
						usuarios.insertar(a, new VOUsuario(a, new Lista<>(), new Lista<>(), null));
					}
					VOUsuario u = usuarios.darValor(a);
					u.getRatings().add(rating);
					if(u.getPrimeraRevision()==null||u.getPrimeraRevision().compareTo(date)>0){
						u.setPrimeraRevision(date);
					}

				}catch (NumberFormatException e) {
					continue;
				}catch (IllegalStateException e) {
					continue;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void cargarTags(String rutaTags, boolean q) {
		try(BufferedReader br = new BufferedReader(new FileReader(rutaTags))){
			tags = 0;
			String linea = br.readLine();
			while((linea = br.readLine())!=null){
				try{
					String[] a = linea.split(",");
					int userId = Integer.parseInt(a[0]);
					int movieId = Integer.parseInt(a[1]);
					String tag = a[2];
					long timestamp = Long.parseLong(a[3]);
					Date date = new Date(timestamp);
					VOTag t = new VOTag(tag, timestamp, userId, movieId);
					if(q){peliculas.darValor(movieId).getTagsAsociados().add(t);}
					if(!usuarios.tieneLlave(userId)){
						usuarios.insertar(userId, new VOUsuario(userId, new Lista<>(), new Lista<>(), null));
					}
					VOUsuario u = usuarios.darValor(userId);
					u.getTags().add(t);
					if(u.getPrimeraRevision()==null||u.getPrimeraRevision().compareTo(date)>0){
						u.setPrimeraRevision(date);
					}
					tags++;
				}catch(NumberFormatException e){
					continue;
				}catch (IllegalStateException e) {
					continue;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void cargarGeneros(){
		for (VOPelicula p : peliculas.valores()) {
			for(String g: p.getGenerosAsociados()){
				g=g.trim();
				if(!generos.tieneLlave(g)){
					generos.insertar(g, new ArbolBalanceado<>());
				}
				generos.darValor(g).put(p.getFechaPublicacion(), p);
			}
		}
	}

	public void SimilitudCoseno(VOPelicula p1){
		for(VOPelicula p2: peliculas.valores()){
			Lista<Double> u1 = new Lista<>();
			Lista<Double> u2 = new Lista<>();

			for(VORating r1:p1.getRatings()){
				for(VORating r2:p2.getRatings()){
					if(r1.getIdUsuario()==r2.getIdUsuario()){
						u1.add(r1.getRating());
						u2.add(r2.getRating());
						break;
					}
				}
			}
			if(u1.size()<3){
				p1.getSimilitudes().add(new VOSimilitud(p2.getIdPelicula(), 0.0));
			}
			else{
				double a = 0;
				double b = 0;
				for (Double k : u1) {
					a+=(k*k);
				}
				for (Double k : u2) {
					b+=(k*k);
				}
				a=a/(double)u1.size();
				b=b/(double)u2.size();

				double c = 0;
				for (int i = 0; i < u1.size(); i++) {
					c+=(u1.get(i)*u2.get(i));
				}

				p1.getSimilitudes().add(new VOSimilitud(p2.getIdPelicula(), c/(a*b)));
			}
		}
	}
	
	public int sizeTagsSR(){
		return tags;
	}
	
	public int sizeusersSR(){
		return usuarios.darTamanio();
	}
	
	public int sizeMoviesSR(){
		return peliculas.darTamanio();
	}
	
	public void registrarSolicitudRecomendacion(int idUsuario, String ruta){
		if(usuarios.tieneLlave(idUsuario)){
			cola.add(usuarios.darValor(idUsuario));
		}else{
			idUsuario = -1;
			try {
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(new FileReader(ruta));
				JSONObject n = (JSONObject) obj;
				JSONObject usuario = (JSONObject) n.get("request");
				JSONArray array = (JSONArray) usuario.get("ratings");
				String nombre =  (String) usuario.get("user_name");
				Lista<VORating> ratings = new Lista<>();
				for (Object object : array) {
					JSONObject jsonObject = (JSONObject)object;
					double rating;
					int idPelicula = (new Long((long) jsonObject.get("item_id"))).intValue();
					try{
						rating = (double) jsonObject.get("rating");
					}
					catch (ClassCastException e) {
						long i = (long) jsonObject.get("rating");
						String s = i+"";
						rating = Double.parseDouble(s);
					}
					if(peliculas.tieneLlave(idPelicula)){
						ratings.add(new VORating(idUsuario, idPelicula, rating, 0));
					}
				}
				Date a = new Date();
				a.setYear(3100);
				Lista<VOTag> tags = new Lista<>();
				tags.add(new VOTag(nombre, 0, idUsuario, 0));
				cola.add(new VOUsuario(idUsuario, ratings, tags, a));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	
	}
	
	public void generarRespuestasRecomendaciones(){	
		Lista<Lista<VOSimilitud>> listas = new Lista<>();
		Lista<Recomendacion> hilos = new Lista<>();
		Lista<VOUsuario> us = new Lista<>();

		int n = 0;
		while (!cola.isEmpty()&&n<10) {
			listas.add(new Lista<>());
			us.add(cola.max());
			hilos.add(new Recomendacion(us.get(n), listas.get(n), false));
			hilos.get(0).start();
			n++;
		}
		for (Recomendacion r : hilos) {
			try {
				r.join();
			} catch (Exception e) {
				continue;
			}
		}
		JSONObject obj = new JSONObject();
		JSONObject obj1 = new JSONObject();
		JSONArray usua = new JSONArray();
		for (int i = 0; i < listas.size(); i++) {
			JSONObject obj2 = new JSONObject();
			if(us.get(i).getIdUsuario()!=-1){obj2.put("user_id", us.get(i).getIdUsuario());}
			else{obj2.put("user_id", us.get(i).getTags().get(0).getTag());}
			JSONArray reco = new JSONArray();
			for (VOSimilitud s : listas.get(i)) {
				JSONObject obj3 = new JSONObject();
				obj3.put("item_id", s.getIdPelicula());
				obj3.put("p_rating", s.getSimilitud());
				reco.add(obj3);
			}
			obj2.put("recommendations", reco);
			usua.add(obj2);
		}
		obj1.put("response_users", usua);
		obj.put("response", obj1);
		Date d = new Date();
		ultimaRuta = "Recomendaciones_<"+d.getDate()+"-"+(d.getMonth()+1)+"-"+(d.getYear()+1900)+"_"+d.getHours()+"_"+d.getMinutes()+"_"+d.getSeconds()+">.json";
		try (FileWriter file = new FileWriter(ultimaRuta)) {
			file.write(obj.toJSONString());
		}
		catch(Exception e){
			
		}
	}
	
	public VOGeneroPelicula peliculasGeneroPorFechas(String genero, Date fechaInicial, Date fechaFinal){	
		VOGeneroPelicula gp = new VOGeneroPelicula(genero, new Lista<>());
		if(generos.tieneLlave(genero)){
			ArbolBalanceado<Date, VOPelicula> arbol = generos.darValor(genero);
			for(Date p :arbol.llaves(fechaInicial, fechaFinal)){
				gp.getPeliculas().add(arbol.get(p));
			}
			return gp;
		}
		return null;
	}
	
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating){	
		if(usuarios.tieneLlave(idUsuario)){
			if(peliculas.tieneLlave(idPelicula)){
				VORating rt = new VORating(idUsuario, idPelicula, rating, System.currentTimeMillis());
				VOUsuario u = usuarios.darValor(idUsuario);
				VOPelicula p = peliculas.darValor(idPelicula);
				double arriba = 0;
				double abajo = 0;
				for(VORating r: u.getRatings()){
						double sim = p.getSimilitudes().get(r.getIdPelicula()).getSimilitud();
						arriba += (sim*r.getRating());
						abajo += sim;
				}
				rt.setError(Math.abs((arriba/abajo)-rating));
				u.getRatings().add(rt);
				p.getRatings().add(rt);
			}
		}
	}
	
	public Lista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario){	
		Lista<VOUsuarioPelicula> l = new Lista<>();
		if(usuarios.tieneLlave(idUsuario)){
			VOUsuario u = usuarios.darValor(idUsuario);
			Lista<Integer> rated = new Lista<>();
			Lista<Integer> tagged = new Lista<>();
			for (int i = 0; i < u.getRatings().size(); i++) {
				rated.add(u.getRatings().get(i).getIdPelicula());
			}
			for (int i = 0; i < u.getTags().size(); i++) {
				rated.add(u.getTags().get(i).getIdPelicula());
			}
			Lista<VOSimilitud> ratingsSistema = new Lista<>();
			Recomendacion y = new Recomendacion(u, ratingsSistema, true);
			y.start();
			try {
				y.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for (VOSimilitud s : ratingsSistema) {
				VOPelicula p = peliculas.darValor(s.getIdPelicula());
				VOUsuarioPelicula n = new VOUsuarioPelicula(p.getTitulo(), -1, s.getSimilitud(), -1, new Lista<>(), idUsuario);
				if(rated.indexOf(p.getIdPelicula())>-1){
					VORating rt = u.getRatings().get(rated.indexOf(p.getIdPelicula()));
					n.setErrorRating(rt.getError());
					n.setRatingUsuario(rt.getRating());
				}
				if(tagged.indexOf(p.getIdPelicula())>-1){
					for(VOTag t : u.getTags()){
						if(t.getIdPelicula()==p.getIdPelicula()){
							n.getTags().add(t);
						}
					}
				}
				l.add(n);
			}
			return l;
		}
		return null;
	}
	public void req6(){	
	}
	public void req7(){	
	}
	public void req8(){	
	}
	public void req9(){	
	}
	public void req10(){	
	}
	public void req11(){	
	}
	
	public static EncadenamientoSeparadoTH<Integer, VOPelicula> getPeliculas(){
		return peliculas;
	}
	
	public static EncadenamientoSeparadoTH<Integer, VOUsuario> getUsuarios(){
		return usuarios;
	}

	public static MaxHeapCP<VOUsuario> getCola() {
		return cola;
	}

	public static EncadenamientoSeparadoTH<String, ArbolBalanceado<Date, VOPelicula>> getGeneros() {
		return generos;
	}
	
	
}
