package estructuras;

import java.util.Iterator;

public class Queue<H> implements Iterable<H>{

	private class Nodo<H>{


		private H elemento;
		private Nodo siguiente;
		private Nodo anterior;

		public Nodo(H elemento){
			this.elemento = elemento;
		}

		public Nodo getSiguiente() {
			return siguiente;
		}

		public void setSiguiente(Nodo siguiente) {
			this.siguiente = siguiente;
		}

		public Nodo getAnterior() {
			return anterior;
		}

		public void setAnterior(Nodo anterior) {
			this.anterior = anterior;
		}
		
		public H getElemento() {
			return elemento;
		}

	}

	private Nodo<H> primero;
	private Nodo<H> ultimo;

	public Queue(){	
	}
	
	public void enqueue(H item){
		if(primero==null){
			primero = new Nodo<H>(item);
			ultimo = primero;
		}
		else{
			Nodo<H> a = primero;
			primero = new Nodo<H>(item);
			primero.setSiguiente(a);
			a.setAnterior(primero);
		}
	}

	public H dequeue(){
		if(ultimo!=null){
			Nodo<H> a = ultimo;
			if(ultimo.getAnterior()!=null){
				ultimo=(ultimo.getAnterior());
				ultimo.setSiguiente(null);
			}
			else{
				ultimo=null;
				primero = null;
			}
			return a.getElemento();
		}
		return null;
	}
	
	public boolean isEmpty(){
		return primero!=null?false:true;
	}

	public int size(){
		int n = 0;
		Nodo<H> a = primero;
		while(a!=null){
			n++;
			a = a.getSiguiente();
		}
		return n;
	}

	public Iterator<H> iterator(){
		return new Iterator<H>() {
			Nodo<H> a = primero;
			public boolean hasNext() {
				return a!=null?true:false;
			}
			public H next() {
				Nodo<H> b = a;
				a = a.getSiguiente();
				return b.getElemento();
			}
		};
	}
}
