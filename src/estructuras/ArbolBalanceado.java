package estructuras;

import java.util.NoSuchElementException;

public class ArbolBalanceado<K extends Comparable<K>, V> {

    private static final boolean ROJO  = true;
    private static final boolean BLACK = false;

    private Nodo raiz;     


    private class Nodo {
        private K key;           
        private V val;         
        private Nodo left, right;  
        private boolean color;    
        private int size;          

        public Nodo(K key, V val, boolean color, int size) {
            this.key = key;
            this.val = val;
            this.color = color;
            this.size = size;
        }
    }

 
    public ArbolBalanceado() {
    }

 
    private boolean isRed(Nodo x) {
        if (x == null) return false;
        return x.color == ROJO;
    }


    private int size(Nodo x) {
        if (x == null) return 0;
        return x.size;
    } 

    
    public int size() {
        return size(raiz);
    }


    public boolean isEmpty() {
        return raiz == null;
    }


    public V get(K key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        return get(raiz, key);
    }

    private V get(Nodo x, K key) {
        while (x != null) {
            int cmp = key.compareTo(x.key);
            if      (cmp < 0) x = x.left;
            else if (cmp > 0) x = x.right;
            else              return x.val;
        }
        return null;
    }

    
    public boolean contains(K key) {
        return get(key) != null;
    }


    public void put(K key, V val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }

        raiz = put(raiz, key, val);
        raiz.color = BLACK;
    }

    private Nodo put(Nodo h, K key, V val) { 
        if (h == null) return new Nodo(key, val, ROJO, 1);

        int cmp = key.compareTo(h.key);
        if      (cmp < 0) h.left  = put(h.left,  key, val); 
        else if (cmp > 0) h.right = put(h.right, key, val); 
        else              h.val   = val;

        // fix-up any right-leaning links
        if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
        if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
        if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);
        h.size = size(h.left) + size(h.right) + 1;

        return h;
    }


    public void deleteMin() {
        if (isEmpty()) throw new NoSuchElementException("BST underflow");

        if (!isRed(raiz.left) && !isRed(raiz.right))
            raiz.color = ROJO;

        raiz = deleteMin(raiz);
        if (!isEmpty()) raiz.color = BLACK;
    }

    private Nodo deleteMin(Nodo h) { 
        if (h.left == null)
            return null;

        if (!isRed(h.left) && !isRed(h.left.left))
            h = moveRedLeft(h);

        h.left = deleteMin(h.left);
        return balance(h);
    }



    public void deleteMax() {
        if (isEmpty()) throw new NoSuchElementException("BST underflow");

        if (!isRed(raiz.left) && !isRed(raiz.right))
            raiz.color = ROJO;

        raiz = deleteMax(raiz);
        if (!isEmpty()) raiz.color = BLACK;
    }

    private Nodo deleteMax(Nodo h) { 
        if (isRed(h.left))
            h = rotateRight(h);

        if (h.right == null)
            return null;

        if (!isRed(h.right) && !isRed(h.right.left))
            h = moveRedRight(h);

        h.right = deleteMax(h.right);

        return balance(h);
    }

    public void delete(K key) { 
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");
        if (!contains(key)) return;

        if (!isRed(raiz.left) && !isRed(raiz.right))
            raiz.color = ROJO;

        raiz = delete(raiz, key);
        if (!isEmpty()) raiz.color = BLACK;
    }

    private Nodo delete(Nodo h, K key) { 

        if (key.compareTo(h.key) < 0)  {
            if (!isRed(h.left) && !isRed(h.left.left))
                h = moveRedLeft(h);
            h.left = delete(h.left, key);
        }
        else {
            if (isRed(h.left))
                h = rotateRight(h);
            if (key.compareTo(h.key) == 0 && (h.right == null))
                return null;
            if (!isRed(h.right) && !isRed(h.right.left))
                h = moveRedRight(h);
            if (key.compareTo(h.key) == 0) {
                Nodo x = min(h.right);
                h.key = x.key;
                h.val = x.val;
                h.right = deleteMin(h.right);
            }
            else h.right = delete(h.right, key);
        }
        return balance(h);
    }


    private Nodo rotateRight(Nodo h) {
        Nodo x = h.left;
        h.left = x.right;
        x.right = h;
        x.color = x.right.color;
        x.right.color = ROJO;
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    private Nodo rotateLeft(Nodo h) {
        Nodo x = h.right;
        h.right = x.left;
        x.left = h;
        x.color = x.left.color;
        x.left.color = ROJO;
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    private void flipColors(Nodo h) {
        h.color = !h.color;
        h.left.color = !h.left.color;
        h.right.color = !h.right.color;
    }


    private Nodo moveRedLeft(Nodo h) {
        flipColors(h);
        if (isRed(h.right.left)) { 
            h.right = rotateRight(h.right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    private Nodo moveRedRight(Nodo h) {
        flipColors(h);
        if (isRed(h.left.left)) { 
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }


    private Nodo balance(Nodo h) {

        if (isRed(h.right))                      h = rotateLeft(h);
        if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
        if (isRed(h.left) && isRed(h.right))     flipColors(h);

        h.size = size(h.left) + size(h.right) + 1;
        return h;
    }


    public int height() {
        return height(raiz);
    }
    private int height(Nodo x) {
        if (x == null) return -1;
        return 1 + Math.max(height(x.left), height(x.right));
    }


    public K min() {
        if (isEmpty()) throw new NoSuchElementException("called min() with empty symbol table");
        return min(raiz).key;
    } 

    private Nodo min(Nodo x) { 

        if (x.left == null) return x; 
        else                return min(x.left); 
    } 


    public K max() {
        if (isEmpty()) throw new NoSuchElementException("called max() with empty symbol table");
        return max(raiz).key;
    } 


    private Nodo max(Nodo x) { 

        if (x.right == null) return x; 
        else                 return max(x.right); 
    } 

    private K select(int k) {
        if (k < 0 || k >= size()) {
            throw new IllegalArgumentException("called select() with invalid argument: " + k);
        }
        Nodo x = select(raiz, k);
        return x.key;
    }


    private Nodo select(Nodo x, int k) {

        int t = size(x.left); 
        if      (t > k) return select(x.left,  k); 
        else if (t < k) return select(x.right, k-t-1); 
        else            return x; 
    } 


    private int rank(K key) {
        if (key == null) throw new IllegalArgumentException("argument to rank() is null");
        return rank(key, raiz);
    } 


    private int rank(K key, Nodo x) {
        if (x == null) return 0; 
        int cmp = key.compareTo(x.key); 
        if      (cmp < 0) return rank(key, x.left); 
        else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
        else              return size(x.left); 
    } 


 

    private void llaves(Nodo x, Queue<K> queue, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) llaves(x.left, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
        if (cmphi > 0) llaves(x.right, queue, lo, hi); 
    } 

    public Iterable<K> llaves() {
        if (isEmpty()) return new Queue<K>();
        return llaves(min(), max());
    }


    public Iterable<K> llaves(K lo, K hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to llaves() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to llaves() is null");

        Queue<K> queue = new Queue<K>();
        llaves(raiz, queue, lo, hi);
        return queue;
    }

  
    public int size(K lo, K hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to size() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to size() is null");

        if (lo.compareTo(hi) > 0) return 0;
        if (contains(hi)) return rank(hi) - rank(lo) + 1;
        else              return rank(hi) - rank(lo);
    }



   
    private boolean isBST() {
        return isBST(raiz, null, null);
    }


    private boolean isBST(Nodo x, K min, K max) {
        if (x == null) return true;
        if (min != null && x.key.compareTo(min) <= 0) return false;
        if (max != null && x.key.compareTo(max) >= 0) return false;
        return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
    } 

    private boolean isSizeConsistent() { return isSizeConsistent(raiz); }
    private boolean isSizeConsistent(Nodo x) {
        if (x == null) return true;
        if (x.size != size(x.left) + size(x.right) + 1) return false;
        return isSizeConsistent(x.left) && isSizeConsistent(x.right);
    } 


    private boolean isRankConsistent() {
        for (int i = 0; i < size(); i++)
            if (i != rank(select(i))) return false;
        for (K key : llaves())
            if (key.compareTo(select(rank(key))) != 0) return false;
        return true;
    }


    
    private boolean is23() { return is23(raiz); }
    private boolean is23(Nodo x) {
        if (x == null) return true;
        if (isRed(x.right)) return false;
        if (x != raiz && isRed(x) && isRed(x.left))
            return false;
        return is23(x.left) && is23(x.right);
    } 


    private boolean isBalanced() { 
        int black = 0;   
        Nodo x = raiz;
        while (x != null) {
            if (!isRed(x)) black++;
            x = x.left;
        }
        return isBalanced(raiz, black);
    }


    private boolean isBalanced(Nodo x, int black) {
        if (x == null) return black == 0;
        if (!isRed(x)) black--;
        return isBalanced(x.left, black) && isBalanced(x.right, black);
    } 

    public boolean check(){
    	return isRankConsistent()&&isBalanced()&&is23()&&isSizeConsistent()&&isBST();
    }

}
