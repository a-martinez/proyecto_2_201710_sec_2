package test;

import estructuras.ArbolBalanceado;
import junit.framework.TestCase;

public class testArbolBalanceado extends TestCase{
	
	private ArbolBalanceado<Integer, Integer> ab = new ArbolBalanceado<>();;
	
	private void setUp1(){
		ab = new ArbolBalanceado<>();
		for (int i = 0; i < 100; i++) {
			ab.put(i, i);
		}
	}
	
	public void testPut(){
		ab.put(0, 0);
		for (int i = 0; i < 1000; i++) {
			ab.put(i, i);
			assertEquals("El arbol no tiene el tamano correcto", i+1, ab.size());
			assertTrue("La altura del arbol no es correcta -", ab.height()<=Math.floor(Math.log(i+1)/Math.log(2)));
			assertTrue("La altura del arbol no es correcta +", ab.height()>=Math.floor(Math.log(i+1)/Math.log(3)));
		}
	}
	public void testGet(){
		setUp1();
		for(Integer k : ab.llaves()){
			assertEquals("El item retornado no es el correcto", k, ab.get(k));
		}
	}
	public void testGetMax(){
		setUp1();
		assertEquals("El item retornado no es el maximo", new Integer(99), (Integer)ab.max());
	}
	public void testGetMin(){
		setUp1();
		assertEquals("El item retornado no es el minimo", new Integer(0), (Integer)ab.min());
	}
	public void testContains(){
		setUp1();
		for(Integer k : ab.llaves()){
			assertTrue(ab.contains(k));
		}
		assertTrue(!ab.contains(100));
	}
	public void testIsEmpty(){
		assertTrue(ab.isEmpty());
		setUp1();
		assertTrue(!ab.isEmpty());
	}
	public void testDelete(){
		setUp1();
		int i = 100;
		for(Integer k : ab.llaves()){
			i--;
			ab.delete(k);
			assertTrue(ab.size()==i);
			if(i==0){
				break;
			}
			assertTrue("La altura del arbol no es correcta -", ab.height()<=Math.floor(Math.log(i+1)/Math.log(2)));
			assertTrue("La altura del arbol no es correcta +", ab.height()>=Math.floor(Math.log(i+1)/Math.log(3)));
		}
	}
	
	
}
