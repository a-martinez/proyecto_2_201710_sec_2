package vos;

import java.io.Serializable;
import java.util.Date;

import estructuras.Lista;

public class VOUsuario implements Serializable, Comparable<VOUsuario>{

	private static final long serialVersionUID = 4L;
	private int idUsuario;	
	private Lista<VORating> ratings;
	private Lista<VOTag> tags;
	private Date primeraRevision;

	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Lista<VORating> getRatings() {
		return ratings;
	}
	public void setRatings(Lista<VORating> ratings) {
		this.ratings = ratings;
	}
	public Lista<VOTag> getTags() {
		return tags;
	}
	public void setTags(Lista<VOTag> tags) {
		this.tags = tags;
	}
	public Date getPrimeraRevision() {
		return primeraRevision;
	}
	public void setPrimeraRevision(Date primeraRevision) {
		this.primeraRevision = primeraRevision;
	}
	public VOUsuario(int idUsuario, Lista<VORating> ratings, Lista<VOTag> tags, Date primeraRevision) {
		super();
		this.idUsuario = idUsuario;
		this.ratings = ratings;
		this.tags = tags;
		this.primeraRevision = primeraRevision;
	}
	@Override
	public int compareTo(VOUsuario o) {
		if(primeraRevision.getYear()>o.getPrimeraRevision().getYear()){
			return 1;
		}
		if(primeraRevision.getYear()<o.getPrimeraRevision().getYear()){
			return -1;
		}
		else if(primeraRevision.getMonth()>o.getPrimeraRevision().getMonth()){
			return 1;
		}
		else if(primeraRevision.getMonth()<o.getPrimeraRevision().getMonth()){
			return -1;
		}
		else if(getRatings().size()>o.getRatings().size()){
			return 1;
		}
		else if(getRatings().size()<o.getRatings().size()){
			return -1;
		}
		else if(getTags().size()>o.getTags().size()){
			return 1;
		}
		else if(getTags().size()<o.getTags().size()){
			return -1;
		}
		return 0;
	}

	
	
	

	
}
