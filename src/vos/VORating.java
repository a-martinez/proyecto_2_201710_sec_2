package vos;

import java.io.Serializable;

public class VORating implements Serializable{
	
	private static final long serialVersionUID = 2L;
	private int idUsuario;
	private int idPelicula;
	private double rating;
	private long timestamp;
	private double error;
	
	public double getError() {
		return error;
	}
	public void setError(double error) {
		this.error = error;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public VORating(int idUsuario, int idPelicula, double rating, long timestamp) {
		super();
		this.idUsuario = idUsuario;
		this.idPelicula = idPelicula;
		this.rating = rating;
		this.timestamp = timestamp;
		error = 0;
	}

	
	

}
