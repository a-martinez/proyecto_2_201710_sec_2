package vos;

import java.io.Serializable;

public class VOSimilitud implements Serializable{

	private static final long serialVersionUID = 3L;
	private double similitud;
	private int idPelicula;
	public double getSimilitud() {
		return similitud;
	}
	public void setSimilitud(double similitud) {
		this.similitud = similitud;
	}
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public VOSimilitud(int idPelicula, double similitud) {
		this.similitud = similitud;
		this.idPelicula = idPelicula;
	}
	
}
