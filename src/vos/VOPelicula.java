package vos;

import java.io.Serializable;
import java.util.Date;

import estructuras.Lista;

public class VOPelicula implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int idPelicula; 
	private String titulo;
	private double promedioRatings;
	private Date fechaPublicacion;

	private Lista<VOTag> tagsAsociados;
	private String[] generosAsociados;
	private Lista<VORating> ratings;
	private Lista<VOSimilitud> similitudes;
	
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public Lista<VOTag> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(Lista<VOTag> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public String[] getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(String[] generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public Lista<VORating> getRatings() {
		return ratings;
	}
	public void setRatings(Lista<VORating> ratings) {
		this.ratings = ratings;
	}
	
	public Lista<VOSimilitud> getSimilitudes() {
		return similitudes;
	}
	public void setSimilitudes(Lista<VOSimilitud> similitudes) {
		this.similitudes = similitudes;
	}
	public VOPelicula(int idPelicula, String titulo, double promedioRatings, Date fechaPublicacion,
			Lista<VOTag> tagsAsociados, String[] generosAsociados, Lista<VORating> ratings,
			Lista<VOSimilitud> similitudes) {
		super();
		this.idPelicula = idPelicula;
		this.titulo = titulo;
		this.promedioRatings = promedioRatings;
		this.fechaPublicacion = fechaPublicacion;
		this.tagsAsociados = tagsAsociados;
		this.generosAsociados = generosAsociados;
		this.ratings = ratings;
		this.similitudes = similitudes;
	}
	
	
}
