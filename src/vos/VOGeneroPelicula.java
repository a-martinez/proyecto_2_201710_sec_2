package vos;

import estructuras.Lista;

public class VOGeneroPelicula {
	
	public VOGeneroPelicula(String genero, Lista<VOPelicula> peliculas) {
		this.genero = genero;
		this.peliculas = peliculas;
	}

	private String genero;
	
	private Lista<VOPelicula> peliculas;

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Lista<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(Lista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 

}
