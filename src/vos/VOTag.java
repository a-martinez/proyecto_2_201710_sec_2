package vos;

import java.io.Serializable;

public class VOTag implements Serializable{
	private static final long serialVersionUID = 8L;
	private String tag;
	private long timestamp;
	private int idUsuario;
	private int idPelicula;
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public VOTag(String tag, long timestamp, int idUsuario, int idPelicula) {
		super();
		this.tag = tag;
		this.timestamp = timestamp;
		this.idUsuario = idUsuario;
		this.idPelicula = idPelicula;
	}
	
}
